FROM node:lts-alpine

# Set timezone to UTC by default
RUN ln -sf /usr/share/zoneinfo/Etc/UTC /etc/localtime

# Update repository indexes
RUN apk update

# Install aws-cli
RUN apk -Uuv add aws-cli

# Clean apk cache
RUN rm /var/cache/apk/*

CMD ["/bin/sh"]
